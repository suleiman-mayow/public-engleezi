import React, { Component } from 'react'
import './default.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Layout from '../components/layout'
import About from '../components/Layouts/About/About'
import AboutData from '../Data/aboutData'
import SEO from '../components/seo'
import Keywodrs from '../Data/keywords'

class AboutPage extends Component {
  render() {
    const data = AboutData.Ar
    const seoKeywords = Keywodrs
    return (
      <Layout>
        <SEO
          title="About Us"
          keywords={seoKeywords}
        />
        <About data={data} />
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
      </Layout>
    )
  }
}
export default AboutPage
