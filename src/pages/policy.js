import React from 'react'
import './default.css'
import Layout from '../components/layout'
import Policy from '../components/Layouts/Policy/Policy'
import 'bootstrap/dist/css/bootstrap.min.css'
import SEO from '../components/seo'
const PolicyPage = () => (
  <Layout>
    <SEO title="Policy" keywords={[`Engleezi`, `Website`, `react`]} />
    <Policy />
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
  </Layout>
)

export default PolicyPage
