import React, { Component } from 'react'
import './default.css'
import Layout from '../components/layout'
import Home from '../components/Layouts/Home/Home'
import 'bootstrap/dist/css/bootstrap.min.css'
import SEO from '../components/seo'
import IndexData from '../Data/HomeData'
import '../components/fonts.css'
import Keywodrs from '../Data/keywords'

class IndexPage extends Component {
  render() {
    const data = IndexData.Ar
    const seoKeywords = Keywodrs
    return (
      <Layout>
        <SEO
          title="Home"
          keywords={seoKeywords}
        />
        <Home data={data} />
      </Layout>
    )
  }
}

export default IndexPage
