import React, { Component } from 'react'
import './default.css'
import Layout from '../components/layout'
import Teachers from '../components/Layouts/Teachers/Teacher'
import 'bootstrap/dist/css/bootstrap.min.css'
import TeacherData from '../Data/teacherData'
import SEO from '../components/seo'
import Keywodrs from '../Data/keywords'

class TeachersPage extends Component {
  render() {
    const data = TeacherData.Ar
    const seoKeywords = Keywodrs
    return (
      <Layout>
        <SEO
          title="Teachers"
          keywords={seoKeywords}
        />
        <Teachers data={data} />
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
      </Layout>
    )
  }
}
export default TeachersPage
