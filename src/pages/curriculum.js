import React, { Component } from 'react'
import './default.css'
import Layout from '../components/layout'
import Curriculum from '../components/Layouts/Curriculum/Curriculum'
import 'bootstrap/dist/css/bootstrap.min.css'
import SEO from '../components/seo'
import CurriculumData from '../Data/curriculumData'
import Keywodrs from '../Data/keywords'

class CurriculumPage extends Component {
  render() {
    const data = CurriculumData.Ar
    const seoKeywords = Keywodrs
    return (
      <Layout>
        <SEO
          title="Our Curriculum"
          keywords={seoKeywords}
        />
        <Curriculum data={data} />
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
      </Layout>
    )
  }
}
export default CurriculumPage
