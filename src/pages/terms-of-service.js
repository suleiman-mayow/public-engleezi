import React from 'react'
import './default.css'
import Layout from '../components/layout'
import Terms from '../components/Layouts/Terms/Terms'
import 'bootstrap/dist/css/bootstrap.min.css'
import SEO from '../components/seo'
const TermsPage = () => (
  <Layout>
    <SEO
      title="Master Service Agreement"
      keywords={[`Engleezi`, `Website`, `react`]}
    />
    <Terms />
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
  </Layout>
)

export default TermsPage
