import React, { Component } from 'react'
import './default.css'
import Layout from '../components/layout'
import Support from '../components/Layouts/Support/Support'
import 'bootstrap/dist/css/bootstrap.min.css'
import SupportData from '../Data/supportData'
import SEO from '../components/seo'
import Keywodrs from '../Data/keywords'

class SupportPage extends Component {
  render() {
    const data = SupportData.Ar
    const seoKeywords = Keywodrs
    return (
      <Layout>
        <SEO title="Support" keywords={seoKeywords} />
        <Support data={data} />

        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
      </Layout>
    )
  }
}
export default SupportPage
