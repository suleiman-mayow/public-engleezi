import React, { Component } from 'react'
import './default.css'
import Layout from '../components/layout'
import Start from '../components/Layouts/HowToStart/Start'
import 'bootstrap/dist/css/bootstrap.min.css'
import SEO from '../components/seo'
import StartData from '../Data/startData'
import Keywodrs from '../Data/keywords'

class StartPage extends Component {
  render() {
    const data = StartData.Ar
    const seoKeywords = Keywodrs
    return (
      <Layout>
        <SEO
          title="How to Start"
          keywords={seoKeywords}
        />
        <Start data={data} />
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
      </Layout>
    )
  }
}
export default StartPage
