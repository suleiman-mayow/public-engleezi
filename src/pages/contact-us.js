import React, { Component } from 'react'
import './default.css'
import Layout from '../components/layout'
import Contact from '../components/Layouts/ContactUs/Contact'
import 'bootstrap/dist/css/bootstrap.min.css'
import SEO from '../components/seo'
import ContactData from '../Data/contactData'
import Keywodrs from '../Data/keywords'

class ContactPage extends Component {
  render() {
    const data = ContactData.Ar
    const seoKeywords = Keywodrs
    return (
      <Layout>
        <SEO
          title="Contact Us"
          keywords={seoKeywords}
        />
        <Contact data={data} />
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }} />
      </Layout>
    )
  }
}
export default ContactPage
