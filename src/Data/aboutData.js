const aboutData = {
  Ar: {
    Bannar: {
      headerTextH1: 'معلومات عنا',
      paragraphTextP:
        'إنجليزي هي منصة تعليمية مبتكرة على أحدث مستوى ، تسهل دروس اللغة الإنجليزية الفردية و ذو جودة عالية للأطفال. فصولنا و دروسنا ليست فقط ممتعة ، بل مصممة أيضًا لتكون في متناول الجميع ومناسبة للجميع. إنجليزي يمكن طفلك الآن من تعلم اللغة الإنجليزية بكل راحة في منزلك',
    },
  },
}
export default aboutData
