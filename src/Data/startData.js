const startData = {
  Ar: {
    Bannar: {
      headerTextH1: 'كيف تبدأ ؟',
      paragraphTextP: 'الأمر بغايه السهولة مثل واحد واتنين وثلاثة ',
      buttonText: 'سجل الآن',
    },
    Steps: {
      step1: 'سجّل دخولك على برنامجنا واختر الدرس',
      step2: ' اختر الوقت والتاريخ المناسبين',
      step3: 'ابدأ التعلّم',
    },
    Bonus: {
      bonusHeading: 'علاوة على ذلك',
      bonusText:
        '  سجّل الآن و احصل على درس مجّاني للشهر الأول! ليس ذلك فقط، بإمكانك الربح من خلال دعوة اصدقائك الى انجليزي ',
      bonusImportant: '(يكون ذلك ممكناً حتى 10 دعوات في السنة كحد أقصى) ',
      bonusButton: ' أنا مهتم',
    },
  },
}
export default startData
