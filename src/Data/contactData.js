const curriculumData = {
  Ar: {
    Bannar: {
      headerTextH1: 'تحتاج الى مساعدة ؟',
      paragraphTextP: 'لا تتردد في التواصل معنا على ',
      link: 'info@myengleezi.com',
    },
    Form: {
      header: 'تواصل معنا',
      text: 'تواصل مع فريقنا على',
      link: 'info@myengleezi.com',
      label: {
        name: 'الإسم الكامل',
        namePlaceholder: '',
        email: 'البريد الالكتروني',
        emailPlaceholder: '',
        phone: 'رقم الهاتف',
        phonePlaceholder: '',
        massage: 'الرسالة',
        send: ' إرسال ',
      },
    },
  },
}
export default curriculumData
