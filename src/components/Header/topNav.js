import React from 'react'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap'

class TopNav extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false,
    }
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    })
  }
  render() {
    let linksMarkup = this.props.links.map((link, index) => {
      let linkMarkup = link.active ? (
        <NavLink className="grey-co" href={link.link} className="active">
          {link.label}
        </NavLink>
      ) : (
        <NavLink className="" href={link.link}>
          {link.label}
        </NavLink>
      )
      return <NavItem key={index}>{linkMarkup}</NavItem>
    })

    return (
      <div>
        <Navbar color="light" light expand="lg">
          <div className="container">
            <NavbarBrand className="brand float-left" href="/">
              <img
                src={this.props.logo}
                className="headerLogo"
                alt="Engleezi Logo"
              />
            </NavbarBrand>
            <NavbarToggler className="blue-bg" onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav
                className="ml-auto navbarMenu wt-bg pl-3 pr-3 pt-3 pb-3"
                navbar
              >
                {linksMarkup}
              </Nav>
              <Nav
                className="md-margin-8px-top ml-auto navbarMenu blue-bg pl-3 pr-3 pt-3 pb-3"
                navbar
              >
                <NavItem>
                  <NavLink
                    className="white-co"
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    target="_blank"
                  >
                    الدخول
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className="white-co"
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    target="_blank"
                  >
                    التسجيل
                  </NavLink>
                </NavItem>

                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle className="white-co" nav caret>
                    اللغات
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      <NavLink
                        className="grey-co"
                        href="https://en.myengleezi.com"
                        target="_blank"
                      >
                        English
                      </NavLink>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      <NavLink className="grey-co" href="/" target="_blank">
                        عربي
                      </NavLink>
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </div>
        </Navbar>
      </div>
    )
  }
}
export default TopNav
