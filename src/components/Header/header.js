import React, { Component } from 'react'
import './header.css'
import logo from './logo.png'
import TopNav from './topNav'

class Header extends Component {
  render() {
    return (
      <header style={headStyle}>
        <TopNav links={this.props.links} logo={logo} />
      </header>
    )
  }
}

const headStyle = { position: 'absolute', width: '100%', zIndex: '5' }

export default Header
