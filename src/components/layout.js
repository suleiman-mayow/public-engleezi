import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import Header from './Header/header'
import Footer from './Footer/footer'
import Chat from './chat'
import './layout.css'
import './fonts.css'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Header
          links={arlinks}
          siteTitle={data.site.siteMetadata.title}
          style={{ zIndex: 5 }}
        />

        <main>{children}</main>
        <Chat />
        <Footer />
      </>
    )}
  />
)

const arlinks = [
  { label: 'تواصل معنا', link: '/contact-us/' },
  { label: 'الدعم', link: '/support/' },
  { label: 'كيف تبدأ', link: '/how-to-start/' },
  { label: 'مناهجنا', link: '/curriculum/' },
  { label: 'معلمينا', link: '/teachers/' },
  { label: 'الرئيسية', link: '/' },
]

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
