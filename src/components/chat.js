import React from 'react'
import MessengerCustomerChat from 'react-messenger-customer-chat'

function Chat() {
  return (
    <div>
      <MessengerCustomerChat pageId="141174630082637" appId="291857224821659" />
    </div>
  )
}

export default Chat
