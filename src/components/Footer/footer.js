import { Link } from 'gatsby'
import React from 'react'
import './footer.css'
import footerBg from './footerBg.svg'
import subscribeBackground from './subscribeBg.png'
import engleeziLogo from './engleeziLogo.png'
import { Icon } from 'react-icons-kit'
import { ic_mail_outline, ic_stay_current_portrait } from 'react-icons-kit/md/'
import { facebook, youtube, twitter } from 'react-icons-kit/fa'
import { arrowLeft2 } from 'react-icons-kit/icomoon/arrowLeft2'

const Footer = () => (
  <footer>
    <div
      className="banner-creative bg-img cover-background no-padding-bottom"
      style={divStyle}
    >
      <div className="container footer-pad">
        <div className="row">
          <div className="box" style={{ display: 'flex' }}>
            <div className="col-lg-5 col-md-4 auto-margin" id="textBox">
              <p
                className="text-right kufi"
                style={{ textAlign: 'justify', color: '#525051' }}
              >
                قم بتجربة الدروس التي نقدّمها مجانًا لكي تأخذ فكرة عن مناهجنا
                الدراسيّة الفريدة! نعم! كل ما عليك فعله هو التسجيل و احصل على
                درسًا كاملًا بالمجّان
              </p>
            </div>
            <div
              className="col-lg-7 col-md-8 col-sm-12 small-view"
              style={subscribeBg}
            >
              <div
                className="email-box  wow fadeInUp"
                style={{
                  visibility: 'visible',
                  animationDelay: '0.3s',
                  animationName: 'fadeInUp',
                  top: '35%',
                  right: '2%',
                }}
              >
                <div className="input float-right col-md-8">
                  <a
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    rel="noopener noreferrer"
                    target="_blank"
                    className="position-relative col-md-12 butn"
                  >
                    <span className="text-center kufi">أشترك الان</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="divider" />
        <div className="row">
          <div className="col-md-4 padding-six-bottom">
            <img
              src={engleeziLogo}
              alt="Engleezi Logo file"
              className="footerImage"
            />
            <p className="small-disapear text-right kufi fontSizeP">
              اللغة الإنجليزية هي لغة مهمة يتكلم بها الكثير من الناس في جميع
              أنحاء العالم. على الرغم من أن التعلم قد يكون صعباً بالنسبة للبعض،
              نحن في إنجليزي نعتقد أن تعلم اللغة الإنجليزية يجب أن يكون ممتعاً
              ومثيراً
            </p>
          </div>
          <div className="col-md-2 padding-six-bottom small-disapear">
            <h5 className="padding-four-bottom white-co text-right kufi fontSizeh5 ">
              الوصول السريع
            </h5>
            <Link className="quickNav text-right" to="/about-us/">
              <p className="kufi fontSizeP">معلومات عنا</p>
            </Link>

            <a
              className="quickNav padding-two-bottom text-right"
              href="https://blog.myengleezi.com/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <p className="kufi fontSizeP">مدونة انجليزي</p>
            </a>
            <a
              className="quickNav padding-two-bottom text-right"
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.youtube.com/channel/UCFUvVaTVIMb0IqKK2lIMC0g/"
            >
              <p className="kufi fontSizeP">مصادرنا </p>
            </a>
            <Link
              className="quickNav padding-two-bottom text-right"
              to="/terms-of-service/"
            >
              <p className="kufi fontSizeP"> شروط الخدمة</p>
            </Link>

            <Link
              className="quickNav padding-two-bottom text-right"
              to="/policy/"
            >
              <p className="kufi fontSizeP">سياسة الخصوصية</p>
            </Link>
          </div>
          <div className="col-md-1" />
          <div className="col-md-3 padding-six-bottom">
            <h5 className="padding-two-bottom white-co text-right kufi fontSizeh5 ">
              تواصل معنا{' '}
            </h5>
            <div className="row">
              <p className="col-md-10 col-sm-9 text-right fontSizeP">
                {' '}
                <a href="mailto:info@myengleezi.com" className="quickNav ">
                  info@myengleezi.com
                </a>
              </p>
              <span className="col-md-2 col-sm-3">
                <Icon size={32} icon={ic_mail_outline} />
              </span>
              <p className="col-md-10 col-sm-9 text-right fontSizeP">
                {' '}
                <a href="tel:info@myengleezi.com" className="quickNav ">
                  +6011 2114 6268
                </a>{' '}
              </p>
              <span className="col-md-2 col-sm-3">
                <Icon size={32} icon={ic_stay_current_portrait} />
              </span>
            </div>
          </div>
          <div className="col-md-2 text-right">
            <h5 className="padding-four-bottom padding-five-right white-co kufi text-right fontSizeh5">
              تابعونا
            </h5>
            <span className="margin-five-right text-right">
              <a
                href="https://www.facebook.com/myengleezi"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  size={32}
                  icon={facebook}
                  style={{ color: 'white' }}
                  className="socialIcons social margin-three-bottom text-left"
                />
              </a>
            </span>
            <span className="margin-five-right ">
              <a
                href="https://www.twitter.com/myengleezi"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  size={32}
                  icon={twitter}
                  style={{ color: 'white' }}
                  className="socialIcons social margin-three-bottom text-left"
                />
              </a>
            </span>
            <span className="margin-five-right">
              <a
                href="https://www.youtube.com/channel/UCFUvVaTVIMb0IqKK2lIMC0g/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon
                  size={32}
                  style={{ color: 'white' }}
                  icon={youtube}
                  className="socialIcons social margin-three-bottom text-left"
                />
              </a>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div className="copyRight text-center" style={rightsReserved}>
      {' '}
      © {new Date().getFullYear()} كل الحقوق محفوظة{' '}
    </div>

    <div className="promotion-box" style={promotionBar}>
      <a href="https://student.myengleezi.com/HumanResource/login.aspx" target="_blank"
        rel="noopener noreferrer">
        <div className="row co-white ">
          <div className="container">
            <div className="row d-flex flex-md-row-reverse row">
              <p className="col-md-7 col-sm-7 co-white kufi text-right pr-4">
                احصل على درس تجريبي مجاني اليوم في اللغة الإنجليزية + 25٪ خصم على أي مشتريات في السنة الأولى
              </p>
              <span className="col-md-4 col-sm-5 text-center">
                <button className="button-light" style={promoButton}>
                  <span className="co-blue">
                    <Icon
                      size={25}
                      icon={arrowLeft2}
                      className="fas fa-arrow-right text-right display-inline col-md-4"
                    />
                  </span>
                  <span
                    className=" kufi text-button text-black col-md-8"
                    style={{ color: 'black' }}
                  >
                    سجل الان
            </span>
                </button>
              </span>
            </div>
          </div>

        </div>

      </a>
    </div>


  </footer >
)

const divStyle = {
  backgroundImage: 'url(' + footerBg + ')',
}
const subscribeBg = {
  backgroundImage: 'url(' + subscribeBackground + ')',
  padding: 0,
  backgroundRepeat: 'no-repeat',
  backgroundSize: '120%',
  borderRadius: '12px',
}
const rightsReserved = {
  position: 'relative',
  zIndex: '0',
}
const promotionBar = {
  position: 'fixed',
  bottom: '0'
}
const promoButton = {
  height: '48px',
  width: '184px'
}

export default Footer
