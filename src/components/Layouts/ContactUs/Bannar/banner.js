import React from 'react'
import bannerRight from './Banner.png'

const mailto = 'mailto:'
const Banner = props => (
  <section
    style={{
      width: '100%',
      paddingTop: '0',
    }}
  >
    <div className="main-banner-area">
      <div className="right-bg">
        <img
          src={bannerRight}
          className="img-fluid float-right width-100"
          alt=""
        />
      </div>
      <div className="header-text">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-md-12 col-sm-12">
              <h1
                className="text-right no-letter-spacing co-blue kufi wow fadeInUp"
                data-wow-delay=".1s"
              >
                {props.header}
              </h1>
              <p
                className=" kufi text-right wow fadeInUp no-letter-spacing"
                data-wow-delay=".2s"
              >
                {props.para} <br />
                <a style={styleAnchor} href={mailto + props.link}>
                  {props.link}
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

const styleAnchor = {
  color: 'inherit',
  textDecoration: 'inherit',
}
export default Banner
