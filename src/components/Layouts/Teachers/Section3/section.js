import React from 'react'
import sectionImg from './section.png'

const Services = props => (
  <section>
    <div className="container">
      <div className="row">
        <div className=" col-md-5 col-sm-12">
          <img
            src={sectionImg}
            alt="Get Instant results with Engleezi"
            className="col-sm-8 col-md-12 "
          />
        </div>
        <div className="offset-md-1 col-md-6 col-sm-12 margin-ten-top">
          <h2 className="text-right margin-five-bottom engleezi-black kufi">
            {props.header}
          </h2>

          <p className="text-right font-weight-400 engleezi-black kufi">
            {props.text}
          </p>
        </div>
      </div>
    </div>
  </section>
)

export default Services
