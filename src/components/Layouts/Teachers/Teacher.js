import React, { Component } from 'react'
import Bannar from './Bannar/banner'
import SectionA from './Section1/section'
import SectionB from './Section2/section'
import SectionC from './Section3/section'

class Teacher extends Component {
  render() {
    const TeacherDataAr = this.props.data
    return (
      <div>
        <Bannar
          header={TeacherDataAr.Bannar.headerTextH1}
          para={TeacherDataAr.Bannar.paragraphTextP}
          button={TeacherDataAr.Bannar.buttonText}
        />
        <SectionA
          header={TeacherDataAr.Section1.header}
          text={TeacherDataAr.Section1.text}
        />
        <SectionB
          header={TeacherDataAr.Section2.header}
          text={TeacherDataAr.Section2.text}
        />
        <SectionC
          header={TeacherDataAr.Section3.header}
          text={TeacherDataAr.Section3.text}
        />
      </div>
    )
  }
}

export default Teacher
