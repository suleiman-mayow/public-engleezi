import React from 'react'
import './whyUs.css'
import ctaBg from './ctaBg.png'
import whyUsBg from './whyUs.png'

const Whyus = props => (
  <section>
    <div className="banner-creative bg-img cover-background " style={divStyle}>
      <div className="container">
        <div className="d-flex flex-md-row-reverse row">
          <div className="col-lg-5 col-md-8 respon-img">
            <img
              src={whyUsBg}
              className="img-fluid float-right width-100"
              alt="Engleezi Values"
            />
          </div>
          <div className="col-lg-6 col-md-12 col-sm-10 margin-twenty-five-top sm-margin-twenty-five-top ">
            <div className="text-right english">
              <h3 className="kufi">{props.reason}</h3>
              <br />

              <ul className=" list-style text-right english margin-30px-bottom">
                {props.list.map((item, index) => {
                  return (
                    <li key={index} className="kufi">
                      {item}
                    </li>
                  )
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)
const divStyle = {
  backgroundImage: 'url(' + ctaBg + ')',
}
export default Whyus
