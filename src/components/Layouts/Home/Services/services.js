import React from 'react'
import service1 from './service1.png'
import service2 from './service2.png'
import service3 from './service3.png'
import './services.css'

const Services = props => (
  <section>
    <div className="container">
      <div className="row ">
        <div className="col-md-4 margin-seven-bottom">
          <div className="container icon-container ">
            <img src={service1} alt="icon3" className="img-center img-hover" />
          </div>
          <div className="padding-twenty-top">
            <h6 className="text-center text-gray kufi">{props.serviceA}</h6>
          </div>
        </div>
        <div className="col-md-4 margin-seven-bottom">
          <div className="container icon-container ">
            <img src={service2} alt="icon2" className="img-center img-hover" />
          </div>
          <div className="padding-twenty-top">
            <h6 className="text-center text-gray kufi">{props.serviceB}</h6>
          </div>
        </div>

        <div className="col-md-4 margin-seven-bottom">
          <div className="container icon-container ">
            <img src={service3} alt="icon1" className="img-center img-hover" />
          </div>
          <div className="padding-twenty-top">
            <h6 className="text-center text-gray kufi">{props.serviceC} </h6>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Services
