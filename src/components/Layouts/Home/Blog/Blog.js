import React from 'react'
import blogPc1 from './blogPc1.png'
import blogPc2 from './blogPc2.png'
import blogPc3 from './blogPc3.png'
import './blog.css'
import { Icon } from 'react-icons-kit'
import { arrowRight2 } from 'react-icons-kit/icomoon/arrowRight2'

function Blog(props) {
  return (
    <section>
      <div className="container">
       
          <h6 className="text-blue text-center kufi"> مدونتنا </h6>
      
        
          <h3 className="kufi section-heading text-center">آخر المنشورات</h3>
     
        <div className="row">
          <div className="col-lg-4 col-md-12  col-sm-12  sm-margin-30px-bottom">
            <div className=" card-shadow card-view">
              <div className="padding-five-bottom">
                <img className="card-img-top" src={blogPc1} alt="blog " />
              </div>
              <p className="margin-two-right text-blue text-right">
                <span> انجليزي | 22 فبراير 2019 </span>
              </p>
              <div className="card-body">
                <h6 className="text-blue kufi text-right kufi">
                  {' '}
                  أفضل طريقة لتعلم اللغة؟
                </h6>
                <p className="card-text text-right kufi" style={textCard}>
                  عندما يتعلم شخص لغة ما، فإن أحد الأسئلة الأكثر شيوعاً هو "ما
                  هي أفضل طريقة لتعلم تلك اللغة؟" بلا شك نحن في إنجليزي سوف نجيب
                  قائلين "الممارسة ثم الممارسة ثم الممارسة!" تعلم اللغة مثله مثل
                  أي مهارة أخرى تحاول اكتسابها. كلما ازداد الوقت الذي تضعه فيها
                  والجهد الذي تبذله كلما حصلت على نتائج أفضل.
                </p>
                <h6 className="text-right">
                  <a
                    href="https://blog.myengleezi.com/best-way-to-learn-english/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="text-blue kufi"> اقرأ المزيد </span>
                  </a>
                  <i className="fas fa-arrow-right text-right col-md-4 display-inline  text-blue" />
                </h6>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-12  col-sm-12 sm-margin-30px-bottom">
            <div className=" card-shadow card-view">
              <div className="padding-five-bottom">
                <img className="card-img-top" src={blogPc2} alt="blog " />
              </div>
              <p className="margin-two-right text-blue text-right kufi">
                <span> انجليزي | 22 فبراير 2019 </span>
              </p>
              <div className="card-body">
                <h6 className="text-blue text-right kufi">
                  {' '}
                  قصتنا - كيف بدأنا{' '}
                </h6>
                <p className="card-text text-right kufi" style={textCard}>
                  كوني من دولة كندا، لغتي الأم هي اللغة الإنجليزية ، حيث أنني
                  درست جميع المراحل التعليمية باللغة الإنجليزية كما أنني أتحدث
                  في المنزل مع عائلتي باللغة الإنجليزية وحتى أنني أحلم باللغة
                  الإنجليزية، فهي بالتأكيد لغتي الأقوى.
                </p>
                <h6 className="text-right">
                  <a
                    href="https://blog.myengleezi.com/our-story/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="text-blue kufi"> اقرأ المزيد </span>
                  </a>
                  <i className="fas fa-arrow-right text-right col-md-4 display-inline text-blue" />
                </h6>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-12 col-sm-12 sm-margin-30px-bottom">
            <div className=" card-view">
              <div className="padding-five-bottom">
                <img className="card-img-top" src={blogPc3} alt="blog " />
              </div>
              <p className="margin-two-right text-blue text-right kufi">
                <span> انجليزي | 22 فبراير 2019 </span>
              </p>
              <div className="card-body">
                <h6 className="text-blue text-right kufi">
                  {' '}
                  مرحباً بك في إنجليزي
                </h6>
                <p className="card-text text-right kufi" style={textCard}>
                  مرحباً بك وشكراً لك على زيارة إنجليزي. اسمي حليمة وأنا عضو
                  مؤسس في مؤسسة إنجليزي وهذه هي قصتنا ...
                </p>
                <h6 className="text-right">
                  <a
                    href="https://blog.myengleezi.com/welcome-to-engleezi/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span className="text-blue text-right kufi">
                      {' '}
                      اقرأ المزيد{' '}
                    </span>
                  </a>
                  <i className="fas fa-arrow-right text-right col-md-4 display-inline  text-blue" />
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="margin-lr-auto margin-tb margin-three-top">
          <a
            href="https://blog.myengleezi.com/"
            rel="noopener noreferrer"
            target="_blank"
          >
            <button className="button-light">
              <span className="text-button  text-black col-md-8 kufi">
                تابع المزيد
                <Icon
                  size={23}
                  icon={arrowRight2}
                  className="fas fa-arrow-right text-right display-inline col-md-4 co-blue"
                />
              </span>
            </button>
          </a>
        </div>
      </div>
    </section>
  )
}

Blog.propTypes = {}
const textCard = {
  height: '50px',
  overflow: 'hidden',
}

export default Blog
