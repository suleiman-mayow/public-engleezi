import React from 'react'
import bannerRight from './Banner.png'
import './bannar.css'

const Banner = () => (
  <section
    style={{
      width: '100%',
      paddingTop: '0',
    }}
  >
    <div className="main-banner-area">
      <div className="right-bg">
        <img
          src={bannerRight}
          className="img-fluid float-right width-100"
          alt=""
        />
      </div>

      <div className="header-text display-fix">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-md-12 col-sm-12">
              <h1
                className="co-blue circularstd-bold no-letter-spacing text-left wow fadeInUp"
                data-wow-delay=".1s"
              >
                Welcome and thank you for visiting Engleezi! My name is Halima
                and I am the co-founder of this organisation. Here’s our story…{' '}
              </h1>
              <p
                className=" poppins text-left no-letter-spacing wow fadeInUp"
                data-wow-delay=".2s"
              >
                English is an important language spoken and understood by many
                across the globe. Though learning it could be difficult for
                some, we at Engleezi believe that learning English should be
                both fun and exciting. Adding to that, our teachers are native
                English speakers with added training to coach and teach children
                residing in the Middle East.
              </p>
              <p
                className=" poppins text-left no-letter-spacing wow fadeInUp hide-1080"
                data-wow-delay=".2s"
              >
                With over 100 classes to follow, our revolutionary platform is
                designed for both teachers and students to converse and learn in
                real-time. This guarantees a unique experience like no other.
                Upon completing the course, we assure that a participant would
                have mastered 50-percent of the English speech, familiarise with
                every sound and achieving the A2 level of English according to
                the CEFL.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Banner
