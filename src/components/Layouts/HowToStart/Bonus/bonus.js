import React, { Component } from 'react'
import { Icon } from 'react-icons-kit'
import { arrowRight2 } from 'react-icons-kit/icomoon/arrowRight2'
import bonus from './bonus.png'

export class Join extends Component {
  render() {
    return (
      <section>
        <div className="container">
          <div className="d-flex flex-md-row-reverse row">
            <div className=" col-md-5 col-sm-12">
              <img
                src={bonus}
                alt="Get Instant results with Engleezi"
                className="col-sm-8 col-md-12 "
              />
            </div>
            <div className="offset-md-1 col-md-6 col-sm-12 margin-ten-top">
              <h2 className="text-right co-blue kufi ">
                {this.props.header}
                <div>&nbsp;</div>
              </h2>
              <p className=" kufi text-right ">{this.props.text}</p>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <p className="text-right kufi">
                <i>{this.props.textItalic}</i>
              </p>

              <div className="divider-full" />
              <div className="fadeInUp">
                <div className="input">
                  <a
                    href="https://student.myengleezi.com/HumanResource/login.aspx"
                    target="_blank"
                  >
                    <button className="button-blue">
                      <span className=" CircularStd-Medium text-button  col-md-8 kufi">
                        {this.props.textButton}
                      </span>
                      <span className="co-white">
                        <Icon
                          size={25}
                          icon={arrowRight2}
                          className="fas fa-arrow-right text-right display-inline col-md-4"
                        />
                      </span>
                    </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Join
