import React from 'react'
import step1 from './step1.png'
import step2 from './step2.png'
import step3 from './step3.png'

const Steps = props => (
  <section>
    <div className="container">
      <div className="row ">
        <div className="col-md-4">
          <div className="col-md-12 margin-ten-bottom">
            <h3 className="kufi text-center engleezi-black kufi"> 1</h3>
          </div>
          <div className="col-md-12 ">
            <img
              src={step1}
              alt="step one with Engleezi"
              className="col-md-12 "
            />
          </div>
          <div className="col-md-12 margin-five-top">
            <h3 className="kufi engleezi-black text-center kufi">
              {props.stepA}{' '}
            </h3>
          </div>
        </div>
        <div className="col-md-4">
          <div className="col-md-12 margin-ten-bottom">
            <h3 className="kufi text-center engleezi-black kufi"> 2</h3>
          </div>
          <div className="col-md-12 ">
            <img
              src={step2}
              alt="step one with Engleezi"
              className="col-md-12 "
            />
          </div>
          <div className="col-md-12 margin-five-top">
            <h3 className="engleezi-black text-center kufi">{props.stepB} </h3>
          </div>
        </div>
        <div className="col-md-4">
          <div className="col-md-12 margin-ten-bottom">
            <h3 className="text-center engleezi-black kufi"> 3</h3>
          </div>
          <div className="col-md-12 ">
            <img
              src={step3}
              alt="step one with Engleezi"
              className="col-md-12 "
            />
          </div>
          <div className="col-md-12 margin-five-top">
            <h3 className="engleezi-black text-center kufi">{props.stepC}</h3>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Steps
