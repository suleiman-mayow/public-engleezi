import React, { Component } from 'react'
import Bannar from './Bannar/banner'
import Steps from './Steps/steps'
import Bonus from './Bonus/bonus'

class Start extends Component {
  render() {
    const StartDataAr = this.props.data
    return (
      <>
        <Bannar
          header={StartDataAr.Bannar.headerTextH1}
          para={StartDataAr.Bannar.paragraphTextP}
          button={StartDataAr.Bannar.buttonText}
        />
        <Steps
          stepA={StartDataAr.Steps.step1}
          stepB={StartDataAr.Steps.step2}
          stepC={StartDataAr.Steps.step3}
        />
        <Bonus
          header={StartDataAr.Bonus.bonusHeading}
          text={StartDataAr.Bonus.bonusText}
          textItalic={StartDataAr.Bonus.bonusImportant}
          textButton={StartDataAr.Bonus.bonusButton}
        />
      </>
    )
  }
}

export default Start
