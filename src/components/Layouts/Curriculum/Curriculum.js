import React, { Component } from 'react'
import Structure from './Structure/structure'
import Result from './Results/result'
import Bannar from './Bannar/banner'

class Curriculum extends Component {
  render() {
    const CurriculumDataAr = this.props.data
    return (
      <>
        <Bannar
          header={CurriculumDataAr.Bannar.headerTextH1}
          para={CurriculumDataAr.Bannar.paragraphTextP}
          button={CurriculumDataAr.Bannar.buttonText}
        />
        <Result
          header={CurriculumDataAr.Result.header}
          text={CurriculumDataAr.Result.text}
        />
        <Structure
          header={CurriculumDataAr.Structure.header}
          text={CurriculumDataAr.Structure.text}
        />
      </>
    )
  }
}
export default Curriculum
