module.exports = {
  siteMetadata: {
    title: `Engleezi - It's so Easy`,
    description: `خدمة Engleezi هي خدمة تعليمية للغة الإنجليزية ممتعة بأسعار معقولة في متناول الجميع ،تهدف إلى جعل طفلك يتحدث اللغة الإنجليزية بطريقة أفضل وأكثر طلاقة. أسلوبنا الفريد من نوعه ونهجنا عبر الإنترنت يجعل أطفالك يتعلمون اللغة الإنجليزية بشكل فردي من مدرس لغته الأم هي اللغة الإنجليزية بكل راحة من منزلكم.
    `,
    author: `Suleiman Mayow <mayow.sullom@gmail.com>`,
    siteUrl: `https://www.myengleezi.com.com`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-catch-links`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-130216069-1',
        // Puts tracking script in the head instead of the body
        head: true,
        anonymize: true,
        respectDNT: true,
        exclude: ['/404/', 'terms-of-service', 'policy', 'blog'],
        sampleRate: 5,
        siteSpeedSampleRate: 10,
        cookieDomain: 'myengleezi.com',
      },
    },
    {
      resolve: 'gatsby-plugin-i18n',
      options: {
        langKeyDefault: 'en',
        useLangKeyLayout: false,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Engleezi-website`,
        short_name: `Engleezi`,
        start_url: `/`,
        background_color: `#0b3880`,
        theme_color: `#0b3880`,
        display: `standalone`,
        icon: `src/images/engleezi-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: 'AW-786517064',

        // Include GTM in development.
        // Defaults to false meaning GTM will only be loaded in production.
        includeInDevelopment: false,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
